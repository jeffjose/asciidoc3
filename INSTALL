= AsciiDoc3 Installation

NOTE: AsciiDoc3 requires *Python 3.x*
to run, it will definitely crash when using Python2.x. Probably there are 
few systems in the real world running with Python 3.0, 3.1, 3.2, 3.3 or 3.4 any more - these versions have reached their end of live. Please consider updating to a newer version! Python 3.5 / 3.6 / 3.7 is a good choice - you don't need the most recent branch at all. Python is installed by
default in most Unix and GNU/Linux distributions. If you don't find an up-to-date version
of Python installed it can be downloaded from the official Python website https://www.python.org/.


[WARNING]
Command line 'python3' obligatory! +
Please note: To run AsciiDoc3, the executable command 'python3' is a must-have. That means, typing 'python3' at the command line starts the Python3 executable. In almost every Unix and GNU/Linux distribution this is a given. When not, you can easily add a symlink python3 -> python3.7 (please take your current version) and add python3 to your PATH ... +
Windows users need probably a 'windows alias for python 3'. +
For further information about AsciiDoc3 and Windows see 
. 

== Obtaining AsciiDoc3
See link:download.html[here].

== Installation
Experienced users should consider to run AsciiDoc3 via PIP in a virtual environment created with link:https://docs.python.org/3/library/venv.html[venv]. +
See AsciiDoc3/PyPI link:pypi.html[here]

=== Unix, GNU/Linux, BSD (and probably OS X, too)
Posix systems (Unix, GNU/Linux, BSD, and assumably OS X) go here.
 
You have two options to install AsciiDoc3: +
- 'system wide' (you need su/sudo/admin rights) +
- 'local' in your home directory, no admin rights required (if Python3 is already installed ...). Doing so, you have to use the 'python3 asciidoc3.py ...' syntax to execute AsciiDoc3.


==== Install system wide (default)
- Download the tarball in any directory with read-write access, e.g. ~/download.
- Open a terminal and change to \~/download ('cd ~/download').
- Deflate the tarball to a new dir, say asciidoc3-3.1.x: 'tar -xzf asciidoc3-3.1.x.tar.gz'.
- You see a new directory ~/download/asciidoc3-3.1.x
- Recommended (but optional): Copy this new directory to your home directory
  and rename it to something like ~/ad3 (so you have less typing labour ...).
- In the terminal: change to the new directory, e.g. ~/ad3.
- Run 'installscript' as root/superuser:
  Ubuntu: 'sudo ./installscript'
  Other POSIX os like Debian, Fedora, BSD: 'su <password>  ./installscript' 
- AsciiDoc3 is ready to use ... continue link:quickstart.html[here].

 

==== Install local
- Download the tarball in any directory with read-write access, e.g. ~/download.
- Open a terminal and change to \~/download (cd ~/download).
- Deflate the tarball to a new dir, say asciidoc3-3.1.x: 'tar -xzf asciidoc3-3.1.x.tar.gz'.
- You see a new directory ~/download/asciidoc3-3.1.x
- Optional: Copy this directory and rename it to ~/asciidoc3 (so you have less typing labour ...)
- In the terminal: change to the new directory, e.g. ~/asciidoc3.
- Start using AsciiDoc3: ... continue link:quickstart.html[here].

- To install 'local' as described before makes is easier to uninstall: just delete ~/ad3.


=== Windows 
Zip Installation: Windows users may go link:windows.html[here]. +
AsciiDoc3 is developed and tested on GNU/Linux. I have only limited access to machines with Windows OS. +

- To install AsciiDoc3 on Windows unzip the Zip file: 'unzip asciidoc3-3.0.x.zip',
- this will create the folder 'asciidoc3-3.0.x' containing the 'asciidoc3.py' and 'a2x3.py' executables along with configuration files,
- try 'python3 asciidoc3.py -n -a icons -o ad3test.html ad3test.txt'... (continue link:quickstart.html[here]).
- see link:windows.html[here]

Asap the zip will be updated to 3.1.0 - and! there will be a PyPI file to be used on Windows!!



=== Testing your installation
See link:quickstart.html[here].


== Uninstall
(this information is also given in file 'UNINSTALL' of the AsciiDoc3 distribution)

=== Uninstall local installation
If you have AsciiDoc3 installed local - that means, you have all files in one 
directory (e.g., \~/asciidoc3) and never ran 'installscript'
(you have used 'python3 asciidoc3.py -a toc -a icons -n mytext.txt' or so):
just delete the directory '~/asciidoc3' (or wherever you have installed the distribution).

=== Uninstall system wide installation
If you ran the AsciiDoc3 'installscript' as root/superuser - that means, you have
directories/files like /etc/asciidoc3/\*.* or /usr/share/asciidoc3/ et al.
(you have used 'asciidoc3 -a toc -a icons -n mytext.txt' or so): +
1. make uninstallscript executable: 'chmod u+x uninstallscript', +
2. 'su/sudo ./uninstallscript', +
3. delete your local files, e.g. in '~/asciidoc3'.  
